# Wardrobify

Team:

* Linda Qian - Shoes
* Kyrstin Jones - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

I will impleemnt a RESTful API, poller, and incorporate React components to the hats microservice. I started by making a Hat model and a LocationVO model which were then used in my views. I also utilized encoders for each of these. I then worked on the poller which worked with my LocationVO to add locations from the wardrobe microservice to my hats. In this case a specific closet for the hats. I then worked on the frontend and created a page to display all of the hats and a page to create a new hat. These both also worked with the wardrobe microservice and LocationVO to display the location of hats in the list and have a dropdown option of locations when creating a new one. I also added a delete and create a new hat button to the list.

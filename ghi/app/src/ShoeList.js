import {Link} from'react-router-dom';
import {useEffect, useState} from 'react';

function ShoeList(props){

    const [shoes, setShoes] = useState([]);

    const fetchData = async() =>{
        const url="http://localhost:8080/api/shoes/";
        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setShoes(data.shoes);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const deleteById = async(id) =>{
        const shoeUrl = `http://localhost:8080/api/shoes/${id}/`
        const response = await fetch(shoeUrl, {method: 'DELETE',});
        if(response.ok){
            setShoes(oldValues => {
                return oldValues.filter(shoe => shoe.id !== id)
            })
        }
    }

    return(
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Bin</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.id}>
                                <td><img src={shoe.picture_url} style={{width:"40px", height:"60px"}}/></td>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin}</td>
                                <td>
                                    <button type="button" className="btn btn-warning" onClick={() => deleteById(shoe.id)}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Link to="/shoes/new" className="btn btn-secondary btn-lg px-4 gap-3">Add New Shoes</Link>
            </div>
        </div>
    )
}

export default ShoeList;

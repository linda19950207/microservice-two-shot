import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatList from './HatList';
import HatForm from './HatForm';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoeList shoes={props.shoes} />}/>
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="hats" element={<HatList hats={props.hats} />} />
          <Route path="hats/new" element={<HatForm />} />
        </Routes>
      </div>
  </BrowserRouter>
  );
}

export default App;

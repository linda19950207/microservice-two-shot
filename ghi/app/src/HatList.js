import React from 'react';
import { useState, useEffect } from 'react'


function HatList(props) {

    const [hats, setHats] = useState([]);

    const fetchData = async() => {
        const url="http://localhost:8090/api/hats/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const deleteByID = async(id) => {
        const hatsUrl = `http://localhost:8090/api/hats/${id}/`;
        const response = await fetch(hatsUrl, {method: 'DELETE',});
        if (response.ok){
            setHats(oldValues => {
                return oldValues.filter(hat => hat.id !== id);
            })
        }
    }
    return (
        <div>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Location</th>
                </tr>
                </thead>
                <tbody>
                {hats?.map(hats => {
                    return (
                        <tr key={hats.id}>
                        <td>{ hats.fabric }</td>
                        <td>{ hats.style_name }</td>
                        <td>{ hats.color }</td>
                        <td>{ hats.location }</td>
                        <td>
                            <button className="btn btn-warning" onClick={() => deleteByID(hats.id)}>Delete</button>
                        </td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <a className="btn btn-info" href='http://localhost:3000/hats/new'>Create a New Hat</a>
            </div>
        </div>
    );
}

export default HatList

import React, {useState, useEffect} from 'react';

function ShoeForm(){
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureURL] = useState('');
    const [bin, setBin] = useState('');

    const handleManufacturerChange = (event) =>{
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModelNameChange = (event) =>{
        const value = event.target.value;
        setModelName(value);
    }

    const handleColorChange = (event) =>{
        const value = event.target.value;
        setColor(value);
    }

    const handleBinChange = (event) =>{
        const value = event.target.value;
        setBin(value);
    }

    const handlePictureURLChange = (event) =>{
        const value = event.target.value;
        setPictureURL(value);
    }

    const handleSubmit = async (event) =>{
        event.preventDefault();
        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;

        const shoesUrl = "http://localhost:8080/api/shoes/";
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        };

        const response = await fetch(shoesUrl, fetchOptions);
        if(response.ok){
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureURL('');
            setBin('');
        }
    }

    const fetchData = async () =>{
        const url = "http://localhost:8100/api/bins/";

        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new pair of shoes</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} value={model_name} placeholder="Model_name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model_name">Model name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureURLChange} value={picture_url} placeholder="Picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a bin</option>
                                {bins.map(bin =>{
                                    return(
                                        <option key={bin.id} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ShoeForm;

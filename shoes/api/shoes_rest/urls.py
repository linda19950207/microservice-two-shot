from django.urls import path
from .views import api_list_shoes, api_show_shoes

urlpatterns = [
    path("shoes/", api_list_shoes, name="list_shoes"),
    path(
        "bins/shoes/", api_list_shoes, name="create_shoes",
    ),
    path("shoes/<int:id>/", api_show_shoes, name="show_shoes")
]

from django.urls import path
from .views import api_list_hats, api_show_hats

urlpatterns = [
    path("hats/", api_list_hats, name="list_hats"),
    path(
        "locations/<int:location_vo_id>/hats/", api_list_hats, name="create_hats",
    ),
    path("hats/<int:id>/", api_show_hats, name="show_hats"),
]

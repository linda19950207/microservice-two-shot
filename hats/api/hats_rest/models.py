from django.db import models


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)
    closet_name = models.CharField(max_length=200)


class Hat(models.Model):
    fabric = models.CharField(max_length=100, null=True)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    picture_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )
